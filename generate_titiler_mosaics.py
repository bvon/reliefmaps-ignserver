import datetime
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from django.utils.encoding import smart_str
import requests
import json
import logging
import os
import uuid
import zipfile
import shutil
import math
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
import threading
import time
from pprint import pprint
import numpy as np
from pathlib import Path
from PIL import Image, ImageEnhance
from io import BytesIO
from cogeo_mosaic.mosaic import MosaicJSON
from cogeo_mosaic.backends.stac import STACBackend
from cogeo_mosaic.backends import FileBackend
from cogeo_mosaic.backends import MosaicBackend
from datetime import datetime, timedelta

from shapely.geometry import shape, Polygon
from shapely.ops import unary_union

from typing import Dict, List, Sequence, Optional
from pygeos import polygons
import mercantile
from concurrent.futures import ThreadPoolExecutor, as_completed

# Optimized Mosaic
def optimized_filter( tile: mercantile.Tile,  dataset: Sequence[Dict], geoms: Sequence[polygons], minimum_tile_cover=None, tile_cover_sort=False, maximum_items_per_tile: Optional[int] = None) -> List:
    """Optimized filter that keeps only one item per grid ID."""
    gridid: List[str] = []
    selected_dataset: List[Dict] = []

    for item in dataset:
        grid = item["id"].split("_")[1]
        if grid not in gridid:
            gridid.append(grid)
            selected_dataset.append(item)

    dataset = selected_dataset

    indices = list(range(len(dataset)))
    if maximum_items_per_tile:
        indices = indices[:maximum_items_per_tile]

    return [dataset[ind] for ind in indices]

def optimized_filter2( tile: mercantile.Tile,  dataset: Sequence[Dict], geoms: Sequence[polygons], minimum_tile_cover=None, tile_cover_sort=False, maximum_items_per_tile: Optional[int] = None) -> List:
    gridid: List[str] = []
    selected_dataset: List[Dict] = []
    bbox = mercantile.bounds(tile)  # Get the bounds
    tile_polygon = Polygon([
    (bbox.west, bbox.north),
    (bbox.east, bbox.north),
    (bbox.east, bbox.south),
    (bbox.west, bbox.south),
    (bbox.west, bbox.north)
    ])

    union_geometry = Polygon()
    
    #add to selected dataset using geometry until the entire tile is covered
    for item in dataset:
        geometry = shape(item["geometry"])  # Convert JSON object to shapely geometry
        union_geometry = unary_union([union_geometry, geometry])
        coverage_percentage = (union_geometry.intersection(tile_polygon).area / tile_polygon.area) * 100
        selected_dataset.append(item)

        if coverage_percentage >= 90:
            break
    
    dataset = selected_dataset
    return dataset

def custom_accessor(feature):
    """Return feature identifier."""
    return feature["assets"]["visual"]["href"]
    # return feature["id"]


def get_stac_backend(cloud_cover,startdate,enddate, z, x, y,optimize=False):
    
    west, south, east, north = getTileBounds(x,y,z)

    sentinel_mosaic = get_stac_backend_from_bounds([west, south, east, north],cloud_cover,startdate,enddate,optimize)
        
    return sentinel_mosaic

def get_stac_backend_from_bounds(bounds,cloud_cover,startdate,enddate,optimize=True):
    stac_url = "https://earth-search.aws.element84.com/v1/search"

    # Specify zoom levels
    minzoom = 11
    maxzoom = 15

    start = datetime.strptime(startdate, "%Y-%m-%d").strftime("%Y-%m-%dT00:00:00Z")
    end = datetime.strptime(enddate, "%Y-%m-%d").strftime("%Y-%m-%dT23:59:59Z")
    #sentinel-2-l2a or sentinel-2-c1-l2a
    query = {"datetime":f"{start}/{end}","bbox":bounds,"query": { "eo:cloud_cover": { "lt": int(cloud_cover) } },"limit":100,"collections":["sentinel-2-l2a"], "sortby": [ { "field": "properties.eo:cloud_cover", "direction": "asc" }, ]}
    
    if optimize:
        sentinel_mosaic = STACBackend(stac_url, query, minzoom, maxzoom, mosaic_options={"accessor": custom_accessor, "asset_filter": optimized_filter2})
    else:
        sentinel_mosaic = STACBackend(stac_url, query, minzoom, maxzoom, mosaic_options={"accessor": custom_accessor})

    # if optimize:
    #     sentinel_mosaic = STACBackend(stac_url, query, minzoom, maxzoom, mosaic_options={"asset_filter": optimized_filter2})
    # else:
    #     sentinel_mosaic = STACBackend(stac_url, query, minzoom, maxzoom)
    
    return sentinel_mosaic

def parent_tile(x, y, current_zoom, target_zoom=8):
    """
    Calculate the parent tile coordinates at the target zoom level.
    
    :param x: X coordinate of the tile at the current zoom level
    :param y: Y coordinate of the tile at the current zoom level
    :param current_zoom: Current zoom level of the tile
    :param target_zoom: Target zoom level (default is 8)
    :return: (x, y) tuple of the tile at the target zoom level
    """
    if current_zoom <= target_zoom:
        raise ValueError("Current zoom level must be greater than target zoom level.")
    
    # Calculate the difference in zoom levels
    zoom_difference = current_zoom - target_zoom
    
    # Calculate the parent tile's coordinates
    parent_x = x // (2 ** zoom_difference)
    parent_y = y // (2 ** zoom_difference)
    
    return parent_x, parent_y

def getTileBounds(x, y, zoom):
    bbox = mercantile.bounds(x, y, zoom)
    
    return bbox.west, bbox.south, bbox.east, bbox.north

def get_last_mondays(num_mondays):
    today = datetime.today()
    mondays = []
    # Start from today and go backwards
    days_ago = (today.weekday() - 0) % 7  # 0 represents Monday
    last_monday = today - timedelta(days=days_ago)
    
    for _ in range(num_mondays):
        mondays.append(last_monday.strftime('%Y-%m-%d'))
        last_monday -= timedelta(days=7)
    
    return mondays


def lat_lon_to_tile(lat, lon, zoom):
    """Convert latitude and longitude to tile coordinates."""
    n = 2 ** zoom
    x_tile = n * ((lon + 180) / 360)
    lat_rad = math.radians(lat)
    y_tile = n * (1 - (math.log(math.tan(lat_rad) + 1 / math.cos(lat_rad)) / math.pi)) / 2
    return int(x_tile), int(y_tile)

def tile_bounds(min_lon,min_lat, max_lon,max_lat, zoom=10):
    """Calculate the tile range within the specified bounds at the given zoom level."""
    
    # Bottom-left corner
    min_tile_x, min_tile_y = lat_lon_to_tile(min_lat, min_lon, zoom)
    # Top-right corner
    max_tile_x, max_tile_y = lat_lon_to_tile(max_lat, max_lon, zoom)
    
    print(min_tile_x, min_tile_y, max_tile_x, max_tile_y)
   
    # Construct a list of tile coordinates
    tiles = []
    for x in range(min_tile_x, max_tile_x + 1):
        for y in range(max_tile_y,min_tile_y  + 1):
            tiles.append([x, y])
    
    return tiles


def generate_mosaic(tile, cloud_cover, startdate, enddate, zoom, four_weeks):
    mosaic_key = f"{tile[0]}_{tile[1]}_{cloud_cover}_{startdate}_{enddate}"
    mosaic_file = "/app/data/" + mosaic_key + ".json"
    print("generating " + mosaic_key)
    if not os.path.exists(mosaic_file):  # generate mosaic only if it doesn't exist
        mosaic = get_stac_backend(cloud_cover, startdate, enddate, zoom, tile[0], tile[1], four_weeks)
        mosaic_doc = mosaic.mosaic_def.model_dump(exclude_none=True) if not four_weeks else mosaic.mosaic_def.dict(exclude_none=True)
        mosaic = MosaicBackend(mosaic_file, mosaic_def=mosaic_doc)
        mosaic.write(overwrite=True)

def process_bounds(bounds, cloud_cover, zoom, num_mondays, threads):
    tiles_to_generate = tile_bounds(bounds[0], bounds[1], bounds[2], bounds[3], zoom)
    print("tiles to generate : " + str(len(tiles_to_generate)))

    last_mondays = get_last_mondays(num_mondays)
    
    with ThreadPoolExecutor(max_workers=threads) as executor:
        futures = []
        
        # Iterate through pairs of Mondays and generate mosaics for single-week range
        for i in range(len(last_mondays) - 1):
            if i + 1 < len(last_mondays):
                startdate = datetime.strptime(last_mondays[i + 1], '%Y-%m-%d').strftime('%Y-%m-%d')
                enddate = datetime.strptime(last_mondays[i], '%Y-%m-%d').strftime('%Y-%m-%d')
                for tile in tiles_to_generate:
                    futures.append(executor.submit(generate_mosaic, tile, cloud_cover, startdate, enddate, zoom, False))
        
        # Iterate through Mondays and generate mosaics for 4-week range
        for i in range(len(last_mondays) - 1):
            if i + 4 < len(last_mondays):
                startdate = datetime.strptime(last_mondays[i + 4], '%Y-%m-%d').strftime('%Y-%m-%d')
                enddate = datetime.strptime(last_mondays[i], '%Y-%m-%d').strftime('%Y-%m-%d')
                for tile in tiles_to_generate:
                    futures.append(executor.submit(generate_mosaic, tile, cloud_cover, startdate, enddate, zoom, True))
        
        # Wait for all threads to complete
        for future in as_completed(futures):
            try:
                future.result()
            except Exception as e:
                print(e)

if __name__ == '__main__':
    cloud_cover = 100
    zoom = 4
    threads = 4
    num_mondays = 78
    
    bounds_list = [
        # [5.9559111595,45.682933398,7.1418273449,46.1301404738], #Annecy
        [-24.3532212421, 36.7530994296, 41.7405287579, 71.5861364317],  # Europe
        [-154.6071274921, 28.0100649294, -50.3688462421, 64.252302433], # North America
        [-176.7555649921, -56.0130323926, 179.0256850079, 72.1603252355], # Rest of World
    ]
    
    print("--started titiler mosaic generation--")
    
    for bounds in bounds_list:
        process_bounds(bounds, cloud_cover, zoom, num_mondays=num_mondays, threads=threads)