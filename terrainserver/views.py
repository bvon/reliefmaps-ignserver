import datetime
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from django.utils.encoding import smart_str
import requests
import json
import logging
import os
import uuid
import zipfile
import shutil
import math
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
import threading
import time
from pprint import pprint
import numpy as np
from pathlib import Path
from PIL import Image, ImageEnhance
from io import BytesIO
from cogeo_mosaic.mosaic import MosaicJSON
from cogeo_mosaic.backends.stac import STACBackend
from cogeo_mosaic.backends import FileBackend
from cogeo_mosaic.backends import MosaicBackend

from typing import Dict, List, Sequence, Optional
from pygeos import polygons
import mercantile

TEMP_DIRECTORY = "/app/data/temptiles"
# IGN_API_KEY = "6x9tvsattis3zoz2ug45aide"

#exemple requête
#https://data.geopf.fr/private/wmts?apikey=bbFfFAFknrpCSrpTsMaNVisWvtlGYfjH&layer=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN25TOUR&style=normal&tilematrixset=PM&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fjpeg&TileMatrix=13&TileCol=4236&TileRow=2917
IGN_API_KEY =  os.environ.get('IGN_API_KEY',"")#bbFfFAFknrpCSrpTsMaNVisWvtlGYfjH ou ign_scan_ws pendant période de transition
IGN_USER_AGENT =  os.environ.get('IGN_USER_AGENT',"") # reliefmaps-app
IGN_USER_REFERER=  os.environ.get('IGN_USER_REFERER',"")# https://www.geoportail.gouv.fr/

IGN_HTTP_SESSION = None


def warmup_sentinel():
    print("SENTINEL WARMUP")


def sentinelmosaic(request,filename):
    #if filename exists, return it
    if os.path.exists("/app/data/"+filename):
        with open("/app/data/"+filename) as f:
            return HttpResponse(f.read())
    
    #else, return empty response
    return HttpResponse("File not found", status=404)

def index(request):
    return HttpResponse("Hello, world. You're at the ignserver index.")


def health(request):
    return HttpResponse("ok")


def getHttpSession():
    global IGN_HTTP_SESSION
    if IGN_HTTP_SESSION is None:
        IGN_HTTP_SESSION = requests.Session()

    return IGN_HTTP_SESSION

# GEOGRAPHICALGRIDSYSTEMS.SLOPES.MOUNTAIN
# ORTHOIMAGERY.ORTHOPHOTOS
# GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN25TOUR
# GEOGRAPHICALGRIDSYSTEMS.MAPS

def google_to_tms(google_x, google_y, zoom_level):
    tms_x = google_x
    tms_y = (2 ** zoom_level) - google_y - 1
    return tms_x, tms_y

def thermiktiles(request, z, x, y):
    # HttpResponse("error : ")
    y = y.replace('.jpg', '')
    y = y.replace('.png', '')

    outputFormat = "png"

    s = getHttpSession()
    overlay_data = None
    
    tms_x, tms_y = google_to_tms(int(x), int(y), int(z))
    r = s.get("https://thermal.kk7.ch/tiles/thermals_all_all/"+str(z)+"/"+str(tms_x)+"/"+str(tms_y)+".png?src=reliefmaps.io")
    base_request_statuscode = r.status_code
    base_data = r.content
    
    if base_request_statuscode == 200:
        final_image = MergeImage(base_data, overlay_data)
    else:
        print("error : "+str(base_request_statuscode))

    response = HttpResponse(content_type="image/"+outputFormat)
    final_image.save(response, outputFormat)
    return response


def norwaytiles(request, z, x, y):
    # HttpResponse("error : ")
    y = y.replace('.jpg', '')
    y = y.replace('.png', '')
    
    outputFormat = "png"
    
    s = getHttpSession()
    overlay_data = None
    
    r = s.get(f"https://cache.kartverket.no/v1/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=topo&STYLE=default&FORMAT=image/png&tileMatrixSet=webmercator&tileMatrix={z}&tileRow={y}&tileCol={x}")
    base_request_statuscode = r.status_code
    base_data = r.content
    
    if base_request_statuscode == 200:
        final_image = MergeImage(base_data, overlay_data)
    else:
        print("error : "+str(base_request_statuscode))

    response = HttpResponse(content_type="image/"+outputFormat)
    final_image.save(response, outputFormat)
    return response

def greenlandtopo(request, z, x, y):
    # Clean up y parameter
    y = y.replace('.jpg', '').replace('.png', '')
    
    # Convert to integers
    z, x, y = int(z), int(x), int(y)
    
    # Set output format
    outputFormat = "png"
    
    # Get the bounding box for the specified tile
    # Convert from web mercator (xyz) to lat/lon and then to EPSG:3857 coordinates
    bounds = mercantile.bounds(x, y, z)
    
    # Convert bounds from WGS84 to EPSG:3857 (Web Mercator)
    # mercantile.xy converts lon/lat to meters in EPSG:3857
    min_x, min_y = mercantile.xy(bounds.west, bounds.south)
    max_x, max_y = mercantile.xy(bounds.east, bounds.north)
    
    # Construct the WMS URL with the calculated BBOX
    wms_url = (
        "https://api.dataforsyningen.dk/wms/gl_aabent_land"
        "?token=e2af4b100e4f10069278062a8228f8b3"
        "&REQUEST=GetMap"
        "&SERVICE=WMS"
        "&VERSION=1.3.0"
        "&FORMAT=image/png"
        "&TRANSPARENT=FALSE"
        "&LAYERS=gl_aabent_land"
        "&TILED=true"
        "&WIDTH=256"
        "&HEIGHT=256"
        "&CRS=EPSG:3857"
        f"&BBOX={min_x},{min_y},{max_x},{max_y}"
    )
    
    # Get HttpSession (assuming this function exists in your code)
    s = getHttpSession()
    
    # Make the request to the WMS server
    r = s.get(wms_url)
    base_request_statuscode = r.status_code
    base_data = r.content
    
    overlay_data = None
    
    if base_request_statuscode == 200:
        # If you have overlay_data handling in your MergeImage function, 
        # you might need to adapt this
        final_image = MergeImage(base_data, overlay_data)
    else:
        print("error : " + str(base_request_statuscode))
        return HttpResponse(f"Error fetching tile: {base_request_statuscode}", status=base_request_statuscode)
    
    # Create and return the HTTP response
    response = HttpResponse(content_type=f"image/{outputFormat}")
    final_image.save(response, outputFormat)
    return response

def greenlandsat(request, z, x, y):
    # Clean up y parameter
    y = y.replace('.jpg', '').replace('.png', '')
    
    # Convert to integers
    z, x, y = int(z), int(x), int(y)
    
    # Set output format
    outputFormat = "png"
    
    # Get the bounding box for the specified tile
    # Convert from web mercator (xyz) to lat/lon and then to EPSG:3857 coordinates
    bounds = mercantile.bounds(x, y, z)
    
    # Convert bounds from WGS84 to EPSG:3857 (Web Mercator)
    # mercantile.xy converts lon/lat to meters in EPSG:3857
    min_x, min_y = mercantile.xy(bounds.west, bounds.south)
    max_x, max_y = mercantile.xy(bounds.east, bounds.north)
    
    # Construct the WMS URL with the calculated BBOX
    wms_url = (
        "https://api.dataforsyningen.dk/wms/gl_satellitfoto"
        "?token=e2af4b100e4f10069278062a8228f8b3"
        "&REQUEST=GetMap"
        "&SERVICE=WMS"
        "&VERSION=1.3.0"
        "&FORMAT=image/png"
        "&TRANSPARENT=FALSE"
        "&LAYERS=ortofoto"
        "&TILED=true"
        "&WIDTH=256"
        "&HEIGHT=256"
        "&CRS=EPSG:3857"
        f"&BBOX={min_x},{min_y},{max_x},{max_y}"
    )
    
    # Get HttpSession (assuming this function exists in your code)
    s = getHttpSession()
    
    # Make the request to the WMS server
    r = s.get(wms_url)
    base_request_statuscode = r.status_code
    base_data = r.content
    
    overlay_data = None
    
    if base_request_statuscode == 200:
        # If you have overlay_data handling in your MergeImage function, 
        # you might need to adapt this
        final_image = MergeImage(base_data, overlay_data)
    else:
        print("error : " + str(base_request_statuscode))
        return HttpResponse(f"Error fetching tile: {base_request_statuscode}", status=base_request_statuscode)
    
    # Create and return the HTTP response
    response = HttpResponse(content_type=f"image/{outputFormat}")
    final_image.save(response, outputFormat)
    return response

def tiles(request, ressource, overlayressource, x, y, z):
    
    headers = {
        'User-Agent': IGN_USER_AGENT,
        'Referer' : IGN_USER_REFERER
    }

    y = y.replace('.jpg', '')
    y = y.replace('.png', '')
    
    outputFormat = "jpeg"

    if int(z) < 10 and ressource == "GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN25TOUR":
        ressource = "GEOGRAPHICALGRIDSYSTEMS.MAPS"

    if ressource == "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2":
        outputFormat = "png"

    s = getHttpSession()
    r = s.get(getIGNUrl(ressource, x, y, z, outputFormat), headers=headers)
    base_request_statuscode = r.status_code
    base_data = r.content
    overlay_data = None

    if overlayressource != "no-overlay" and int(z) > 9:
        r = s.get(getIGNUrl(overlayressource, x, y, z, "png"), headers=headers)
        overlay_data = r.content

    if base_request_statuscode == 200:
        final_image = MergeImage(base_data, overlay_data)
    elif base_request_statuscode == 304:
        final_image = img = Image.new('RGB', (256, 256), (255, 0, 0))
    else:
        final_image = img = Image.new('RGB', (256, 256), (255, 255, 255))

    response = HttpResponse(content_type="image/"+outputFormat)
    # response.content = final_image.tobytes()
    final_image.save(response, outputFormat)
    return response


def MergeImage(base_data, overlay_data):
    if overlay_data is not None:
        background = Image.open(BytesIO(base_data))
        foreground = Image.open(BytesIO(overlay_data))
        foreground = ReduceOpacity(foreground, 0.5)

        background.paste(foreground, (0, 0), foreground)
        return background
    else:
        return Image.open(BytesIO(base_data))


def ReduceOpacity(im, opacity):
    """
    Returns an image with reduced opacity.
    Taken from http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/362879
    """
    assert opacity >= 0 and opacity <= 1
    if im.mode != 'RGBA':
        im = im.convert('RGBA')
    else:
        im = im.copy()
    alpha = im.split()[3]
    alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
    im.putalpha(alpha)
    return im

#ign_scan_ws
def getIGNUrl(ressource, x, y, z, imageformat):
    if ressource == "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2" or ressource == "ORTHOIMAGERY.ORTHOPHOTOS":
        # return "https://data.geopf.fr/private/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix=" + str(z) + "&tilecol=" + str(x) + "&tilerow=" + str(y) + "&layer=" + ressource + "&format=image/"+imageformat+"&style=normal"
        return "https://data.geopf.fr/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix=" + str(z) + "&tilecol=" + str(x) + "&tilerow=" + str(y) + "&layer=" + ressource + "&format=image/"+imageformat+"&style=normal"
    elif ressource == "IGNES":
        return "https://ign.es/wmts/mapa-raster?layer=MTN&style=default&tilematrixset=GoogleMapsCompatible&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/jpeg&&TileMatrix=" + str(z) + "&TileCol=" + str(x) + "&TileRow=" + str(y)
    else:
        return "https://data.geopf.fr/private/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix=" + str(z) + "&tilecol=" + str(x) + "&tilerow=" + str(y) + "&layer=" + ressource + "&format=image/"+imageformat+"&style=normal&apikey=" + IGN_API_KEY
        # return "https://wxs.ign.fr/" + IGN_API_KEY + "/geoportail/wmts?service=WMTS&request=GetTile&version=1.0.0&tilematrixset=PM&tilematrix=" + str(z) + "&tilecol=" + str(x) + "&tilerow=" + str(y) + "&layer=" + ressource + "&format=image/"+imageformat+"&style=normal"

def parent_tile(x, y, current_zoom, target_zoom=8):
    """
    Calculate the parent tile coordinates at the target zoom level.
    
    :param x: X coordinate of the tile at the current zoom level
    :param y: Y coordinate of the tile at the current zoom level
    :param current_zoom: Current zoom level of the tile
    :param target_zoom: Target zoom level (default is 8)
    :return: (x, y) tuple of the tile at the target zoom level
    """
    if current_zoom <= target_zoom:
        raise ValueError("Current zoom level must be greater than target zoom level.")
    
    # Calculate the difference in zoom levels
    zoom_difference = current_zoom - target_zoom
    
    # Calculate the parent tile's coordinates
    parent_x = x // (2 ** zoom_difference)
    parent_y = y // (2 ** zoom_difference)
    
    return parent_x, parent_y


def getTileBounds(x, y, zoom):
    """
    Convert tile x, y at a given zoom level to latitude and longitude.
    
    :param x: X coordinate of the tile
    :param y: Y coordinate of the tile
    :param zoom: Zoom level
    :return: (min_lon, min_lat, max_lon, max_lat) tuple representing the bounding box
    """
    # Number of tiles on one axis at this zoom level
    n = 2.0 ** zoom
    
    # Longitude conversion
    min_lon = x / n * 360.0 - 180.0
    max_lon = (x + 1) / n * 360.0 - 180.0
    
    # Latitude conversion using Web Mercator projection
    min_lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * y / n)))
    max_lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * (y + 1) / n)))
    
    # Convert radians to degrees
    min_lat = math.degrees(min_lat_rad)
    max_lat = math.degrees(max_lat_rad)
    
    return min_lon, min_lat, max_lon, max_lat