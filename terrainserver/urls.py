from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('health', views.health, name='health'),
    path('tiles/<ressource>/<overlayressource>/<z>/<x>/<y>', views.tiles, name='tiles'),
    path('thermiktiles/<z>/<x>/<y>', views.thermiktiles, name='thermiktiles'),
    path('norwaytiles/<z>/<x>/<y>', views.norwaytiles, name='norwaytiles'),
    path('greenlandsat/<z>/<x>/<y>', views.greenlandsat, name='greenlandsat'),
    path('greenlandtopo/<z>/<x>/<y>', views.greenlandtopo, name='greenlandtopo'),
    # path('sentinel2tiles/<cloud_cover>/<startdate>/<enddate>/<z>/<x>/<y>', views.sentinel2tiles, name='sentinel2tiles')
    path('sentinelmosaic/<filename>', views.sentinelmosaic, name='sentinelmosaic')
]