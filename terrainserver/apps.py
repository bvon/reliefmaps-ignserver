from django.apps import AppConfig


class TerrainserverConfig(AppConfig):
    name = 'terrainserver'
    
    def ready(self):
           from . import views
           views.warmup_sentinel()
        #    print("SENTINEL WARMUP")

