import datetime
from datetime import datetime, timedelta
from cogeo_mosaic.backends import MosaicBackend
from typing import Dict, List, Sequence, Optional
from pygeos import polygons
import mercantile
from cogeo_mosaic.mosaic import MosaicJSON
from cogeo_mosaic.backends.stac import STACBackend
from cogeo_mosaic.backends import FileBackend
from cogeo_mosaic.backends import MosaicBackend

start = datetime.strptime("2019-01-01", "%Y-%m-%d").strftime("%Y-%m-%dT00:00:00Z")
end = datetime.strptime("2019-12-11", "%Y-%m-%d").strftime("%Y-%m-%dT23:59:59Z")

geojson = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              -2.83447265625,
              4.12728532324537
            ],
            [
              2.120361328125,
              4.12728532324537
            ],
            [
              2.120361328125,
              8.254982704877875
            ],
            [
              -2.83447265625,
              8.254982704877875
            ],
            [
              -2.83447265625,
              4.12728532324537
            ]
          ]
        ]
      }
    }
  ]
}

query = {
    "collections": ["sentinel-s2-l2a-cogs"],
    "datetime": f"{start}/{end}",
    "query": {
        "eo:cloud_cover": {
            "lt": 3
        },
        "sentinel:data_coverage": {
            "gt": 10
        }
    },
    "intersects": geojson["features"][0]["geometry"],
    "limit": 1000,
    "fields": {
      'include': ['id', 'properties.datetime', 'properties.eo:cloud_cover'],  # This will limit the size of returned body
      'exclude': ['assets', 'links']  # This will limit the size of returned body
    },
    "sortby": [
        {
            "field": "properties.eo:cloud_cover",
            "direction": "desc"
        },
    ]
}

# Simple Mosaic
def custom_accessor(feature):
    """Return feature identifier."""
    return feature["id"]

with STACBackend(
    "stac+https://earth-search.aws.element84.com/v0/search",
    query,
    minzoom=8,
    maxzoom=15,
    mosaic_options={"accessor": custom_accessor},
) as mosaic:
    mosaic_doc = mosaic.mosaic_def.dict(exclude_none=True)

# Optimized Mosaic
def optimized_filter(
    tile: mercantile.Tile,  # noqa
    dataset: Sequence[Dict],
    geoms: Sequence[polygons],
    minimum_tile_cover=None,  # noqa
    tile_cover_sort=False,  # noqa
    maximum_items_per_tile: Optional[int] = None,
) -> List:
    """Optimized filter that keeps only one item per grid ID."""
    gridid: List[str] = []
    selected_dataset: List[Dict] = []

    for item in dataset:
        grid = item["id"].split("_")[1]
        if grid not in gridid:
            gridid.append(grid)
            selected_dataset.append(item)

    dataset = selected_dataset

    indices = list(range(len(dataset)))
    if maximum_items_per_tile:
        indices = indices[:maximum_items_per_tile]

    return [dataset[ind] for ind in indices]


with STACBackend(
    "stac+https://earth-search.aws.element84.com/v0/search",
    query,
    minzoom=8,
    maxzoom=14,
    mosaic_options={"accessor": custom_accessor, "asset_filter": optimized_filter},
) as mosaic:
    mosaic_doc = mosaic

# Write the mosaic
mosaic_file = "/app/data/mymosaic.json.gz"
with MosaicBackend(mosaic_file, mosaic_def=mosaic_doc) as mosaic:
    mosaic.write(overwrite=True)