#!/bin/bash
service cron start

# Start Gunicorn processes
echo "Generating titiler mosaics..."

# ##debug on localhost
# python3.8 -u generate_titiler_mosaics.py &
# echo "Starting Relief Maps Mapdownloader server powered by Django & Gunicorn."
# exec gunicorn reliefmaps.wsgi:application \
#     --bind 0.0.0.0:80 \
#     --workers 1 \
#     --config gunicorn.config.py

# #production config
python3.8 -u generate_titiler_mosaics.py &
echo "Starting Relief Maps Mapdownloader server powered by Django & Gunicorn."
exec gunicorn reliefmaps.wsgi:application \
    --bind 0.0.0.0:80 \
    --workers 64 \
    --config gunicorn.config.py