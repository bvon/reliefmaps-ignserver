import requests
import json

def get_unique_urls(json_url):
    # Fetch the JSON data from the URL
    response = requests.get(json_url)
    
    # Raise an exception if the request was unsuccessful
    response.raise_for_status()

    # Load the JSON data
    data = response.json()

    # Initialize an empty set to store unique URLs
    unique_urls = set()

    # Iterate over each tile's URLs
    for tile_urls in data.get("tiles", {}).values():
        for url in tile_urls:
            unique_urls.add(url)

    return unique_urls

def main():
    url = "https://mapproxy.reliefmaps.io/terrainserver/sentinelmosaic/8_5_100_2024-10-28_2024-11-04.json"
    unique_urls = get_unique_urls(url)

    # Print the number of unique URLs
    print("Number of unique URLs:", len(unique_urls))

    # Optionally, print the unique URLs
    # for url in unique_urls:
    #     print(url)

if __name__ == "__main__":
    main()