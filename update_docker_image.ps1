Invoke-Expression -Command $(aws ecr get-login --no-include-email --region eu-central-1)
docker build -t reliefmaps-ignserver .
docker tag reliefmaps-ignserver:latest 774843963874.dkr.ecr.eu-central-1.amazonaws.com/reliefmaps-ignserver:latest
docker push 774843963874.dkr.ecr.eu-central-1.amazonaws.com/reliefmaps-ignserver:latest

#play beep when finished
[console]::beep(500,250)
[console]::beep(1000,250)
[console]::beep(500,250)
[console]::beep(1000,250)