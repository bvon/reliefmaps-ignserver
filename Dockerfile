##
#
# Download Map tiles and serv it as zip to clients
#
#

# Ubuntu xenial
FROM ubuntu:18.04

LABEL maintainer="batiste.vonderweidt@hotmail.fr"

WORKDIR /app
VOLUME  /app/data

#required for use of add-apt-repository
RUN apt-get update
RUN apt-get install -y software-properties-common

#add required repositories for apt
RUN add-apt-repository -y ppa:ubuntugis/ppa
RUN add-apt-repository ppa:deadsnakes/ppa

#update
RUN apt-get update

#required for use of add-apt-repository
RUN apt-get update
RUN apt-get install -y software-properties-common

#add required repositories for apt
RUN add-apt-repository -y ppa:ubuntugis/ppa
RUN add-apt-repository ppa:deadsnakes/ppa

#update and install dependencies
ENV DEBIAN_FRONTEND=noninteractive 
RUN apt-get update
RUN apt-get install -qqy wget
RUN apt-get install -qqy build-essential

# RUN apt-get install -qqy gdal-bin 
# RUN apt-get install -qqy libgdal-dev 
# RUN apt-get install -qqy libspatialindex-dev 
RUN apt-get install -qqy python3.8 
RUN apt-get install -qqy python3.8-dev 
RUN apt-get install -qqy python-tk 
# RUN apt-get install -qqy idle 
# RUN apt-get install -qqy python-pmw 
#RUN apt-get install -qqy python-imaging 
RUN apt update
# RUN apt-get install -qqy python-opencv 
# RUN apt-get install -qqy python-matplotlib 
# RUN apt-get install -qqy git-all

RUN apt-get install -qqy curl
# RUN curl https://bootstrap.pypa.io/get-pip.py | python3.6
RUN apt install -qqy python3-pip
RUN apt install  -qqy libgeos-dev

RUN python3.8 -m pip install --upgrade pip setuptools wheel
RUN python3.8 -m pip install Cython
RUN python3.8 -m pip install numpy 
RUN python3.8 -m pip install django
RUN python3.8 -m pip install gunicorn
# RUN apt-get install -y zip
# RUN apt-get install -y curl
RUN python3.8 -m pip install requests
RUN python3.8 -m pip install scipy
RUN python3.8 -m pip install django-cors-headers
RUN python3.8 -m pip install cogeo-mosaic --pre
RUN python3.8 -m pip install Pillow
RUN python3.8 -m pip install boto3
RUN python3.8 -m pip install pygeos
RUN python3.8 -m pip install mercantile
RUN python3.8 -m pip install shapely


#Copy py script
COPY . /app

#Add cron
ADD crontab /etc/cron.d/hello-cron
RUN chmod 0644 /etc/cron.d/hello-cron
RUN apt-get -y install cron

CMD cron

EXPOSE 80
ENTRYPOINT ["/bin/bash" , "/app/entrypoint.sh"]